package com.example.assign3.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class FranchiseNotFoundException extends RuntimeException{
    public FranchiseNotFoundException(int id) {
        super("No franchise exists with id "+ id);
    }
}
