package com.example.assign3.services.franchise;

import com.example.assign3.models.entities.Franchise;
import com.example.assign3.models.entities.Movie;
import com.example.assign3.models.entities.MovieCharacter;
import com.example.assign3.services.CRUDService;
import org.springframework.stereotype.Service;
import java.util.Collection;


@Service
public interface FranchiseService extends CRUDService <Franchise, Integer> {
    Collection <Movie> getMoviesFranchise(Franchise franchise);
    Collection <MovieCharacter> getCharactersFranchise(Franchise franchise);

    void updateMoviesFranchise(int[] movieIds, int franchiseId);
}
