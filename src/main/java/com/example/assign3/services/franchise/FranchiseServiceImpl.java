package com.example.assign3.services.franchise;

import com.example.assign3.exceptions.FranchiseNotFoundException;
import com.example.assign3.models.entities.Franchise;
import com.example.assign3.models.entities.Movie;
import com.example.assign3.models.entities.MovieCharacter;
import com.example.assign3.repositories.CharacterRepository;
import com.example.assign3.repositories.FranchiseRepository;
import com.example.assign3.repositories.MovieRepository;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.*;

@Service
public class FranchiseServiceImpl implements FranchiseService {

    private final FranchiseRepository franchiseRepository;
    private final CharacterRepository characterRepository;
    private final MovieRepository movieRepository;
    public FranchiseServiceImpl(FranchiseRepository franchiseRepository, CharacterRepository characterRepository, MovieRepository movieRepository) {
        this.franchiseRepository = franchiseRepository;
        this.characterRepository = characterRepository;
        this.movieRepository = movieRepository;
    }

    @Override
    public Franchise findById(Integer id) {
        return franchiseRepository.findById(id).orElseThrow(() -> new FranchiseNotFoundException(id));
    }

    @Override
    public Collection<Franchise> findAll() {
        return franchiseRepository.findAll();
    }

    @Override
    public void add(Franchise franchise) {
        franchiseRepository.save(franchise);
    }

    @Override
    public void update(Franchise franchise) {
        int id = franchise.getId();
        if(!franchiseRepository.existsById(id)) throw new FranchiseNotFoundException(id);
        franchiseRepository.save(franchise);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        Franchise franchise = franchiseRepository.findById(id).orElseThrow(() -> new FranchiseNotFoundException(id));
        franchise.getMovies().forEach(m -> m.setFranchise(null));
        franchiseRepository.delete(franchise);
    }

    @Override
    @Transactional
    public void delete(Franchise franchise) {
        franchise.getMovies().forEach(m -> m.setFranchise(null));
        franchiseRepository.delete(franchise);
    }

    @Override
    public Collection <Movie> getMoviesFranchise(Franchise franchise){
        return franchise.getMovies();
    }

    /**
     * method that returns all the characters that play in movies of given franchise
     *
     * @param franchise {@link Franchise}
     *
     * @return movieCharacters {@link Collection<MovieCharacter>}
     */
    @Override
    @Transactional
    public Collection <MovieCharacter> getCharactersFranchise(Franchise franchise){
        Set<MovieCharacter> movieCharacters = new HashSet<>();
        Set<Integer> charIds = franchiseRepository.getMovieCharIdsFranchise(franchise.getId());
        for (int id: charIds){
            MovieCharacter movieCharacter = characterRepository.findById(id).get();
            movieCharacters.add(movieCharacter);
        }
        return movieCharacters;
    }

    /**
     * method that updates the movies that belong to a given franchise
     *
     * @param movieIds int[]
     *
     * @param franchiseId {@link Integer}
     *
     */
    @Override
    @Transactional
    public void updateMoviesFranchise(int[] movieIds, int franchiseId){
        Franchise franchise = franchiseRepository.findById(franchiseId).orElseThrow(() -> new FranchiseNotFoundException(franchiseId));
        Set<Movie> oldMovies = franchise.getMovies();
        oldMovies.forEach(o -> o.setFranchise(null));
        for(Integer id: movieIds){
            Movie movie = movieRepository.findById(id).get();
            movie.setFranchise(franchise);
        }
    }
}
