package com.example.assign3.services.movie;

import com.example.assign3.exceptions.CharacterNotFoundException;
import com.example.assign3.exceptions.MovieNotFoundException;
import com.example.assign3.models.entities.Movie;
import com.example.assign3.models.entities.MovieCharacter;
import com.example.assign3.repositories.CharacterRepository;
import com.example.assign3.repositories.MovieRepository;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Service
public class MovieServiceImpl implements MovieService {
    private final MovieRepository movieRepository;
    private final CharacterRepository characterRepository;

    public MovieServiceImpl(MovieRepository movieRepository, CharacterRepository characterRepository) {
        this.movieRepository = movieRepository;
        this.characterRepository = characterRepository;
    }

    @Override
    public Movie findById(Integer id) {
        return movieRepository.findById(id)
                .orElseThrow(() -> new MovieNotFoundException(id));
    }

    @Override
    public Collection<Movie> findAll() {
        return movieRepository.findAll();
    }

    @Override
    public void add(Movie movie) {
        movieRepository.save(movie);
    }

    @Override
    public void update(Movie movie) {
        int id = movie.getId();
        if(!movieRepository.existsById(id)) throw new MovieNotFoundException(id);
        movieRepository.save(movie);
    }

    @Override
    public void deleteById(Integer id) {
        Movie movie = movieRepository.findById(id).orElseThrow(()-> new MovieNotFoundException(id));
        movieRepository.delete(movie);
    }

    @Override
    public void delete(Movie movie) {
        movieRepository.delete(movie);
    }

    /**
     * method that returns all the characters that play in a given movie
     *
     * @param movie {@link Movie}
     * @return movieCharacters {@link Set<MovieCharacter>}
     */
    @Override
    public Set<MovieCharacter> findCharactersMovie(Movie movie){
        Set<Integer> charIds = movieRepository.getCharactersIdMovie(movie.getId());
        Set<MovieCharacter> movieCharacters = new HashSet<>();
        for(int id : charIds){
            MovieCharacter movieCharacter = characterRepository.findById(id).get();
            movieCharacters.add(movieCharacter);
        }
        return movieCharacters;
    }

    /**
     * method that updates all the characters that play in a given movie
     *
     * @param characterIds int[]
     * @param movieId int
     *
     */
    @Override
    @Transactional
    public void updateCharactersMovie(int[] characterIds, int movieId){
        Movie movie = movieRepository.findById(movieId).orElseThrow(() -> new MovieNotFoundException(movieId));
        Set<MovieCharacter> characters = new HashSet<>();
        movie.setCharacters(null);
        for(int charId: characterIds){
            MovieCharacter character = characterRepository.findById(charId).orElseThrow(() -> new CharacterNotFoundException(charId));
            characters.add(character);
        }
        movie.setCharacters(characters);
    }


}
