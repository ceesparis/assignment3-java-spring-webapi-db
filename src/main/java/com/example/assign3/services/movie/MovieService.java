package com.example.assign3.services.movie;

import com.example.assign3.models.entities.Movie;
import com.example.assign3.models.entities.MovieCharacter;
import com.example.assign3.services.CRUDService;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public interface MovieService extends CRUDService <Movie, Integer> {
    Collection <MovieCharacter> findCharactersMovie(Movie movie);
    void updateCharactersMovie(int[] characterIds, int movieId);
}
