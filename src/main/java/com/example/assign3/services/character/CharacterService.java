package com.example.assign3.services.character;

import com.example.assign3.models.entities.MovieCharacter;
import com.example.assign3.services.CRUDService;
import org.springframework.stereotype.Service;


@Service
public interface CharacterService extends CRUDService <MovieCharacter, Integer> {
}
