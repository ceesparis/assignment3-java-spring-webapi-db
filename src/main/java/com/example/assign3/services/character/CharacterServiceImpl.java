package com.example.assign3.services.character;

import com.example.assign3.exceptions.CharacterNotFoundException;
import com.example.assign3.models.entities.MovieCharacter;
import com.example.assign3.repositories.CharacterRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;

@Service
public class CharacterServiceImpl implements CharacterService {

    private final CharacterRepository characterRepository;

    public CharacterServiceImpl(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    public MovieCharacter findById(Integer id) {
        return characterRepository.findById(id).orElseThrow(() -> new CharacterNotFoundException(id));
    }

    @Override
    public Collection<MovieCharacter> findAll() {
        return characterRepository.findAll();
    }

    @Override
    public void add(MovieCharacter character) {
        characterRepository.save(character);
    }

    @Override
    public void update(MovieCharacter character) {
        int id = character.getId();
        if(!characterRepository.existsById(id)) throw new CharacterNotFoundException(id);
        characterRepository.save(character);
    }

    @Override
    @Transactional
    public void deleteById(Integer id){
        MovieCharacter character = characterRepository.findById(id).orElseThrow(() -> new CharacterNotFoundException(id));
        character.getMovies().forEach(m -> m.getCharacters().remove(character));
        characterRepository.delete(character);
    }

    @Override
    public void delete(MovieCharacter character) {
        character.getMovies().forEach(m -> m.getCharacters().remove(character));
        characterRepository.delete(character);
    }
}
