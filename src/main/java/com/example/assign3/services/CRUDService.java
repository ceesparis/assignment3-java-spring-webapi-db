package com.example.assign3.services;

import org.springframework.stereotype.Service;

import java.util.Collection;
public interface CRUDService <T, ID> {
    // Generic CRUD
    T findById(ID id);
    Collection<T> findAll();
    void add(T entity);
    void update(T entity);
    void deleteById(ID id);
    void delete(T entity);
}

