package com.example.assign3.dtos;

import lombok.Data;
import java.util.Set;

@Data
public class MovieDTO {
    private int id;
    private String title;
    private String genre;
    private String director;
    private String poster;
    private String trailer;
    private Integer franchise;
    private Set<Integer> characters;
}
