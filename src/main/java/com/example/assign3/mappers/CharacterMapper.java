package com.example.assign3.mappers;

import com.example.assign3.dtos.CharacterDTO;
import com.example.assign3.models.entities.Movie;
import com.example.assign3.models.entities.MovieCharacter;
import com.example.assign3.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class CharacterMapper {
    @Autowired
    private MovieService movieService;

    @Mapping(target="movies", source="movies")
    public abstract CharacterDTO movieCharToCharDTO(MovieCharacter character);

    @Mapping(target="movies", source="movies")
    public abstract Collection<CharacterDTO> movieCharToCharDTO(Collection<MovieCharacter> characters);
    @Mapping(target="movies", source="movies", qualifiedByName = "mapToMovies")
    public abstract MovieCharacter characterDTOToMovieCharacter(CharacterDTO characterDTO);

    /**
     * method that converts movie-ids to movies
     *
     * @param movieIds {@link Set<Integer>}
     *
     * @return movies {@link Set<Movie>}
     */
    @Named("mapToMovies")
    Set<Movie> mapToMovieCharacters(Set<Integer> movieIds){
        if(movieIds == null) return null;
        return movieIds.stream().map(m -> movieService.findById(m)).collect(Collectors.toSet());
    }

    /**
     * method that converts movies to movie-ids
     *
     * @param movies {@link Set<Movie>}
     *
     * @return movieIds {@link Set<Integer>}
     */
    Set<Integer> map(Set<Movie> movies){
        if (movies == null) return null;
        return movies.stream().map(m -> m.getId()).collect(Collectors.toSet());
    }

}
