package com.example.assign3.mappers;
import com.example.assign3.dtos.MovieDTO;
import com.example.assign3.models.entities.Franchise;
import com.example.assign3.models.entities.Movie;
import com.example.assign3.models.entities.MovieCharacter;
import com.example.assign3.services.character.CharacterService;
import com.example.assign3.services.franchise.FranchiseService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class MovieMapper {
    @Autowired
    private CharacterService characterService;
    @Autowired
    private FranchiseService franchiseService;


    @Mapping(target="franchise", source="franchise", qualifiedByName = "mapFromFranchise")
    @Mapping(target="characters", source="characters", qualifiedByName = "mapFromCharacters")
    public abstract MovieDTO movieToMovieDTO(Movie movie);

    @Mapping(target="franchise", source="franchise", qualifiedByName = "mapFromFranchise")
    @Mapping(target="characters", source="characters", qualifiedByName = "mapFromCharacters")
    public abstract Collection<MovieDTO> movieToMovieDTO(Collection<Movie> movies);

    @Mapping(target="franchise", source="franchise", qualifiedByName = "mapToFranchise")
    @Mapping(target="characters", source="characters", qualifiedByName = "mapToCharacters")
    public abstract Movie movieDTOToMovie(MovieDTO movieDTO);

    /**
     * method that converts characterIds to characters
     *
     * @param characterIds {@link Set<Integer>}
     *
     * @return characters {@link Set<MovieCharacter>}
     */
    @Named("mapToCharacters")
    Set<MovieCharacter> mapToCharacters(Set<Integer> characterIds){
        if (characterIds == null) return null;
        return characterIds.stream().map(i -> characterService.findById(i)).collect(Collectors.toSet());
    }

    /**
     * method that converts characaters to characterIds
     *
     * @param characters {@link Set<MovieCharacter>}
     *
     * @return characterIds {@link Set<Integer>}
     */
    @Named("mapFromCharacters")
    Set<Integer> mapFromCharacters(Set<MovieCharacter> characters){
        if (characters ==  null) return null;
        return characters.stream().map(c -> c.getId()).collect(Collectors.toSet());
    }

    /**
     * method that returns id of a franchise
     *
     * @param franchise {@link Franchise}
     * @return franchiseId {@link Integer}
     */
    @Named("mapFromFranchise")
    Integer mapFromFranchise(Franchise franchise){
        if(franchise == null) return null;
        return franchise.getId();
    }

    /**
     * method that returns franchise by id
     *
     * @param id {@link Integer}
     * @return franchise {@link Franchise}
     */
    @Named("mapToFranchise")
    Franchise mapToFranchise(Integer id){
        if(id == null) return null;
        return franchiseService.findById(id);
    }

}
