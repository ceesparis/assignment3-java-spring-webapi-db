package com.example.assign3.models.entities;

import com.example.assign3.models.entities.Movie;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "Character")
public class MovieCharacter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "full_name", length = 70, nullable = false)
    private String fullName;
    @Column(length = 70)
    private String alias;
    @Column(length=30)
    private String gender;
    @Column(length=200)
    private String picture;

    @ManyToMany(mappedBy = "characters")
    private Set<Movie> movies;
}
