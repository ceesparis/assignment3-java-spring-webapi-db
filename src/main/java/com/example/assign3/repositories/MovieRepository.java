package com.example.assign3.repositories;

import com.example.assign3.models.entities.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Integer> {
    @Query(value = "SELECT character.id FROM character JOIN movie_character" +
            " ON character_id = character.id WHERE movie_id = ?", nativeQuery = true)
    Set<Integer> getCharactersIdMovie(int movieId);

}
