package com.example.assign3.repositories;

import com.example.assign3.models.entities.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface FranchiseRepository extends JpaRepository<Franchise, Integer> {
    @Query(value = "SELECT DISTINCT character.id FROM movie_character +" +
            " JOIN character ON character_id=character.ID +" +
            " JOIN movie ON movie_id = movie.id JOIN franchise " +
            "+ ON movie.franchise_id = franchise.id WHERE franchise.id = ?",
            nativeQuery = true)
    Set<Integer> getMovieCharIdsFranchise(int id);
}
