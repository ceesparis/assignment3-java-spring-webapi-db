package com.example.assign3.controllers;

import com.example.assign3.dtos.CharacterDTO;
import com.example.assign3.mappers.CharacterMapper;
import com.example.assign3.models.util.ApiErrorResponse;
import com.example.assign3.services.character.CharacterService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping(path = "api/character")
public class CharacterController {

    private final CharacterService characterService;

    private final CharacterMapper characterMapper;

    public CharacterController(CharacterService characterService, CharacterMapper characterMapper) {
        this.characterService = characterService;
        this.characterMapper = characterMapper;
    }

    @Operation(summary = "returns all characters")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "Characters successfully returned",
                    content = {@Content(mediaType = "application/json",
                    array = @ArraySchema(schema = @Schema(implementation = CharacterDTO.class)))}),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping
    public ResponseEntity<Collection<CharacterDTO>> findAll(){
        Collection<CharacterDTO> characterDTOS = characterMapper.movieCharToCharDTO(characterService.findAll());
        return ResponseEntity.ok(characterDTOS);
    }

    @Operation(summary = "returns character with id specified in path")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "Character successfully returned",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = CharacterDTO.class))}),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Could not return character: no character with this id",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable int id) {
        CharacterDTO characterDTO = characterMapper.movieCharToCharDTO(characterService.findById(id));
        return ResponseEntity.ok(characterDTO);
    }

    @Operation(summary = "Deletes character with id specified in path")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Character successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Could not delete character: no character with this id",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @DeleteMapping("/{id}")
    public ResponseEntity deleteById(@PathVariable int id) {
        characterService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Updates a character")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Character successfully updated",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CharacterDTO.class))}),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Character not found with supplied ID",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @PutMapping()
    public ResponseEntity update(@RequestBody CharacterDTO characterDTO){
        characterService.update(characterMapper.characterDTOToMovieCharacter(characterDTO));
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Adds character to database")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "Character successfully added to database",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CharacterDTO.class))}),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @PostMapping()
    public ResponseEntity add(@RequestBody CharacterDTO characterDTO){
        characterService.add(characterMapper.characterDTOToMovieCharacter(characterDTO));
        return ResponseEntity.ok(characterDTO);
    }
}