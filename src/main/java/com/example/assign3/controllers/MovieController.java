package com.example.assign3.controllers;

import com.example.assign3.dtos.MovieDTO;
import com.example.assign3.mappers.MovieMapper;
import com.example.assign3.models.util.ApiErrorResponse;
import com.example.assign3.services.movie.MovieService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping(path = "api/movie")
public class MovieController {
    private final MovieService movieService;
    private final MovieMapper movieMapper;

    public MovieController(MovieService movieService, MovieMapper movieMapper) {
        this.movieService = movieService;
        this.movieMapper = movieMapper;
    }

    @Operation(summary = "returns all movies")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "Movies successfully returned",
                    content = {@Content(mediaType = "application/json",
                    array = @ArraySchema(schema = @Schema(implementation = MovieDTO.class)))}),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping
    public ResponseEntity<Collection<MovieDTO>> findAll(){
        return ResponseEntity.ok(movieMapper.movieToMovieDTO(movieService.findAll()));
    }

    @Operation(summary = "returns movie with id specified in path")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "Movie successfully returned",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = MovieDTO.class))}),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Could not return movie: no movie with this id",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable int id) {
        MovieDTO movieDTO = movieMapper.movieToMovieDTO(movieService.findById(id));
        return ResponseEntity.ok(movieDTO);
    }

    @Operation(summary = "Deletes movie with id specified in path")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Movie successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Could not delete movie: no movie with this id",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @DeleteMapping("/{id}")
    public ResponseEntity deleteById(@PathVariable int id) {
        movieService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Updates the characters of a movie")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Movie successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Movie not found with supplied ID",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @PutMapping("/update-characters/{id}")
    public ResponseEntity updateCharacters(@RequestBody int[] charIds, @PathVariable int id){
        movieService.updateCharactersMovie(charIds, id);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Updates a movie")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "Movie successfully updated",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class))}),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Movie not found with supplied ID",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @PutMapping()
    public ResponseEntity update(@RequestBody MovieDTO movieDTO){
        movieService.update(movieMapper.movieDTOToMovie(movieDTO));
        return ResponseEntity.ok(movieDTO);
    }

    @Operation(summary = "adds movie to database")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "Movies successfully added to database",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class))}),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404", description = "specified attribute not found",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class))}),
    })
    @PostMapping()
    public ResponseEntity add(@RequestBody MovieDTO movieDTO){
        movieService.add(movieMapper.movieDTOToMovie(movieDTO));
        return ResponseEntity.ok(movieDTO);
    }

}
