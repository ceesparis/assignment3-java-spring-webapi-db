package com.example.assign3.controllers;

import com.example.assign3.dtos.FranchiseDTO;
import com.example.assign3.mappers.FranchiseMapper;
import com.example.assign3.models.util.ApiErrorResponse;
import com.example.assign3.services.franchise.FranchiseService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping(path = "api/franchise")
public class FranchiseController {
    private final FranchiseService franchiseService;
    private final FranchiseMapper franchiseMapper;

    public FranchiseController(FranchiseService franchiseService, FranchiseMapper franchiseMapper) {
        this.franchiseService = franchiseService;
        this.franchiseMapper = franchiseMapper;
    }

    @Operation(summary = "Returns all franchises")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "Franchises successfully returned",
                    content = {@Content(mediaType = "application/json",
                    array = @ArraySchema(schema = @Schema(implementation = FranchiseDTO.class)))}),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping
    public ResponseEntity<Collection<FranchiseDTO>> findAll(){
        return ResponseEntity.ok(franchiseMapper.franToFranDTO(franchiseService.findAll()));
    }

    @Operation(summary = "returns franchise with id specified in path")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "Franchise successfully returned",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = FranchiseDTO.class))}),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Could not return franchise: no franchise with this id",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("/{id}")
    public ResponseEntity<FranchiseDTO> findById(@PathVariable int id) {
        return ResponseEntity.ok(franchiseMapper.franToFranDTO(franchiseService.findById(id)));
    }

    @Operation(summary = "Deletes franchise with id specified in path")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Franchise successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Could not delete franchise: no franchise with this id",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable int id) {
        franchiseService.deleteById(id);
    }

    @Operation(summary = "Updates the movies of a franchise")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Franchise successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Franchise not found with supplied ID",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @PutMapping("/{id}")
    public ResponseEntity updateMoviesFranchise(@RequestBody int[] newMoviesIds, @PathVariable int id) {
        franchiseService.updateMoviesFranchise(newMoviesIds, id);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Updates a franchise")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "Franchise successfully updated",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class))}),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Franchise not found with supplied ID",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @PutMapping()
    public ResponseEntity update(@RequestBody FranchiseDTO franchiseDTO){
        franchiseService.update(franchiseMapper.franDTOToFran(franchiseDTO));
        return ResponseEntity.ok(franchiseDTO);
    }

    @Operation(summary = "Adds franchise to database")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "200",
                    description = "Franchise successfully added to database",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class))}),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @PostMapping()
    public ResponseEntity add(@RequestBody FranchiseDTO franchiseDTO){
        franchiseService.add(franchiseMapper.franDTOToFran(franchiseDTO));
        return ResponseEntity.ok(franchiseDTO);
    }
}