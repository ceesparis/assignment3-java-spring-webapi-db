# Data access with JDBC

[![pipeline status](https://gitlab.com/ceesparis/assignment3-java-spring-webapi-db/badges/main/pipeline.svg)](https://gitlab.com/ceesparis/assignment3-java-spring-webapi-db/-/commits/main)

<div id="top"></div>

<!-- PROJECT LOGO -->
<br />
<div align="center">
    <img src="spring.png" alt="Logo" width="80" height="80">


<h3 align="center">Web API and database with Spring</h3>

 
</div>



## About The Project

This is the third Java Assignment Project for the Noroff Java Bootcamp.
The assignment consisted of creating a Web API and a movie/character/franchise database with Spring. We then had to containerize the app using Docker and to deploy it to Heroku from a gitlab pipeline.

<p align="right">(<a href="#top">back to top</a>)</p>



### Built With

* [Java](https://www.java.com/nl/)

* [Spring](https://spring.io/)

* [Gradle](https://gradle.org/)

* [PostgreSQL](https://www.postgresql.org/)

* [Intellij](https://www.jetbrains.com/idea/)

* [Docker](https://www.docker.com/)

* [Heroku](https://dashboard.heroku.com/)

<p align="right">(<a href="#top">back to top</a>)</p>



## Getting Started

```sh
git clone url
cd project-directory
./gradlew build
```


## Usage

Build and run from terminal:
```sh
./gradlew bootrun
``` 
You can also deploy the containerized app to heroku via the pipeline on gitlab, and run it from your own heroku app. 
Make sure to include your api-key(HEROKU_API_KEY) as a variable in your gitlab project,
and to change the url in the application.properties file to conform to the database in your heroku app.


## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#top">back to top</a>)</p>


## Maintainers

<p>[@ceesparis](https://gitlab.com/ceesparis)</p>

<p align="right">(<a href="#top">back to top</a>)</p>


## Acknowledgments
I would like to thank Nicholas Lennox and Noroff Accelerate for all the useful documentation and help. 

